-----
## rust

If you are using windows, you need to install [visual studio community](https://www.visualstudio.com/vs/) first

### use [rustup](https://rustup.rs/) to install **nightly** version

the default one is **stable**, we need to use custom install **nightly**

the default one is **stable**, we need to use custom install **nightly**

the default one is **stable**, we need to use custom install **nightly**

```sh
$ export RUSTUP_DIST_SERVER=http://mirrors.ustc.edu.cn/rust-static
$ export RUSTUP_UPDATE_ROOT=http://mirrors.ustc.edu.cn/rust-static/rustup
$ rustup show
$ rustup component list
$ rustup doc
```

### cargo config
```sh
$ cat ~/.cargo/config
#[http]
#proxy = "http://127.0.0.1:1080"
[source.crates-io]
registry = "https://github.com/rust-lang/crates.io-index"
replace-with = 'ustc'
[source.ustc]
registry = "https://mirrors.ustc.edu.cn/crates.io-index/"
```

### install tools by cargo

- `rustfmt` install from source code
```sh
$ git clone https://github.com/rust-lang-nursery/rustfmt.git
$ cd rustfmt
$ cargo install --path .
```
- `racer` install
```sh
$ cargo install racer
```
- `rustsym` install
```sh
$ cargo install rustsym
```
- list cargo installed tools
```sh
$ cargo install --list
cargo-tree v0.17.0:
    cargo-tree.exe
loc v0.4.1:
    loc.exe
racer v2.0.13:
    racer.exe
ripgrep v0.8.1:
    rg.exe
rustfmt-nightly v0.6.0 (file:///D:/project/rust/rustfmt):
    cargo-fmt.exe
    git-rustfmt.exe
    rustfmt-format-diff.exe
    rustfmt.exe
rustsym v0.3.2:
    rustsym.exe

```

### vscode rust config

* plugin rust
* plugin rust(rls)

```json
{
    "rust.rustup": {
        "toolchain": "nightly-x86_64-pc-windows-msvc"
    },
    "rust.mode": "legacy"
}
```
[ref](https://zhuanlan.zhihu.com/p/27782375)


### debug
- rust-gdb (for linux)
- rust-lldb (for macos)
- [vscode](https://www.brycevandyk.com/debug-rust-on-windows-with-visual-studio-code-and-the-msvc-debugger/)


-----
## node

Windows需要 [nvm](https://www.cnblogs.com/hamsterPP/p/8076131.html)

```sh
nvm install 8.9.4
nvm use 8.9.4
nvm node_mirror https://npm.taobao.org/mirrors/node/
nvm npm_mirror https://npm.taobao.org/mirrors/npm/
npm config set registry http://registry.npm.taobao.org/
```
