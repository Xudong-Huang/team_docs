# Learning Rust

a list a useful resources

## rust大神的rust视频介绍
- [vedio1](https://www.youtube.com/watch?v=agzf6ftEsLU)
- [vedio2](https://www.youtube.com/watch?v=lO1z-7cuRYI)

## rust官方文档
- [second-edition](https://doc.rust-lang.org/book/second-edition/index.html)
- [cargo](https://doc.rust-lang.org/cargo/index.html)

## example可以快速看下来，学习会很快
- [exampls](https://doc.rust-lang.org/stable/rust-by-example/)
- [通过例子学-rust(中文版)](https://rustwiki.org/zh-CN//rust-by-example/index.html)

## 一个不错的rust ppt介绍
- [ppt](http://pnkfx.org/presentations/qcon-london2016-deploy/qcon-london2016.html#/)

## 中文版rust Primer
- [rust Primer](https://rustcc.gitbooks.io/rustprimer/content/)

## 24 days of rust
- [24 days of rust](http://zsiciarz.github.io/24daysofrust/)
 
## rust社区大本营（平时没事翻翻看，比较高级，会有一些新的crate发布，技术讨论，每周总结)
- [reddit](https://www.reddit.com/r/rust/)

## 国内rust社区每日新闻(其实就是对reddit的英文总结)
- [每日新闻](https://rust.cc/section/f4703117-7e6b-4caf-aa22-a3ad3db6898f)

## rust学习qq群
- QQ: 303838735

## 你的手边Rust文档：
```sh
$ rustup doc
```
