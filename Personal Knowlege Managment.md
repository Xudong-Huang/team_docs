# Personal Knowledge Management

Do you have a knowledge management system?
How to do you take notes?
What tools do you use?
Where do you get useful information?

Don't fade behind in the INTERNET TIME!

## Knowledge sources

* 微信公众号
* QQ群
* 博客
* [知乎](https://www.zhihu.com/)
* [Reddit](https://www.reddit.com/)
* [InfoQ](http://www.infoq.com/cn/)
* [Hacker News](https://news.ycombinator.com/)
* [GitHub](https://github.com/)
* [Google](https://www.google.com/)
* [百度](https://www.baidu.com/)
* [StackOverflow](https://stackoverflow.com/)

## Rss Tools

* [Feedly](https://feedly.com/)

Collect all your Rss into a single app

Some of My favorite Rss

- [阮一峰的网络日志](https://feedly.com/i/subscription/feed%2Fhttp%3A%2F%2Ffeeds.feedburner.com%2Fruanyifeng)
- [InfoQ-cn](https://feedly.com/i/subscription/feed%2Fhttp%3A%2F%2Fwww.infoq.com%2Fcn%2Frss%2Frss.action%3Ftoken%3DyV5lHoM3uVZTqXcuTdyeDFK4uzcE6XNQ)
- [the morning paper](https://feedly.com/i/subscription/feed%2Fhttp%3A%2F%2Fblog.acolyer.org%2Ffeed%2F)
- [Julia Evans](https://feedly.com/i/subscription/feed%2Fhttp%3A%2F%2Fjvns.ca%2Fatom.xml)
- [IT牛人博客聚合网站](https://feedly.com/i/subscription/feed%2Fhttp%3A%2F%2Fwww.udpwork.com%2Ffeed)
- [并发编程网](https://feedly.com/i/subscription/feed%2Fhttp%3A%2F%2Fifeve.com%2Ffeed%2F)
- [Preshing on Programming](https://feedly.com/i/subscription/feed%2Fhttp%3A%2F%2Fpreshing.com%2Ffeed)

## Cloud Note
you must have one of the cloud note installed at all your devices, phones, PCs
- [Evernote](https://evernote.com/)
国外的老牌笔记
- [Wiznote](http://www.wiz.cn/)
强烈推荐，年费60
- [有道笔记](http://note.youdao.com/)
还不错的一款
- [OneNote](https://www.onenote.com/)
微软家的

* Note is where you save your knowledge (knowledge input)
* Note is how you organize your knowledge (knowledge absorb with tags, folders)
* Note is how you record your own ideas (knowledge output)
* Note is where you can share your knowledge (knowledge share)


