# TrustNote Rust Project Overview

## Goal

* Porting from existing TrustNote js code base
* Re-design from ground and totally re-written in rust language
* Compatible with js implementation so that different language implementation could co-exist in distributed system
* Asynchronous support with IO and computation to fully utilize multi cores of modern CPU 
* Clean architecture with decoupled modules
* Ergonomic API interface design
* High quality of code with strong safety and efficiency


## Current Status
* Basic payment is now working
* Rust version Hub server can sync data with JS Hub server with each other
* Wallet client can work with Rust Hub Server

## Next
* Rust Version Client SDK support
* Rust Version Wallet support including IOT devices
* Pure in memory DAG support to boost the TPS
* Next generation PoW TrustNote algorithm support

## Plan
- [x] Porting existing hub implementation - by 2018-07-31
- [ ] Support x86_64 SDK and command line wallet - by 2018-08-17
- [ ] Memory based data structure for POW based DAG - by 2018-11-17

