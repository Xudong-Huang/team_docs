# Rust Daily Push

我每天会推荐大家一篇rust相关的文章，坚持读下来，你也能成为rust大牛

## 2018-06-04
- [Finding Closure in Rust](http://huonw.github.io/blog/2015/05/finding-closure-in-rust/)

## 2018-06-05
- [Rust's Built-in Traits, the When, How & Why](https://llogiq.github.io/2015/07/30/traits.html)

## 2018-06-06
- [An alternative introduction to Rust](http://words.steveklabnik.com/a-new-introduction-to-rust)

## 2018-06-07
- [Good Practices for Writing Rust Libraries](https://pascalhertleif.de/artikel/good-practices-for-writing-rust-libraries/)

## 2018-06-08
- [Where are you From::from](https://llogiq.github.io/2015/11/27/from-into.html)

## 2018-06-11
- [Comparing ways to concatenate strings in Rust](https://github.com/hoodie/concatenation_benchmarks-rs)

## 2018-06-12
- [String Types in Rust](http://www.suspectsemantics.com/blog/2016/03/27/string-types-in-rust/)

## 2018-06-13
- [How do I convert a &str to a String in Rust?](https://mgattozzi.github.io/2016/05/26/how-do-i-str-string.html)

## 2018-06-14
- [How do I use the Standard Library Macros in Rust? Part 1](https://mgattozzi.github.io/2016/06/01/how-do-i-std-macros.html)

## 2018-06-15
- [Interior mutability in Rust: what, why, how?](https://ricardomartins.cc/2016/06/08/interior-mutability)

## 2018-06-19
- [Interior mutability in Rust, part 2: thread safety](https://ricardomartins.cc/2016/06/25/interior-mutability-thread-safety)

## 2018-06-21
- [Effectively Using Iterators In Rust](https://hermanradtke.com/2015/06/22/effectively-using-iterators-in-rust.html)

## 2018-06-22
- [for loops in Rust](http://xion.io/post/code/rust-for-loop.html)

## 2018-06-27
- [Convenient and idiomatic conversions in Rust](https://ricardomartins.cc/2016/08/03/convenient_and_idiomatic_conversions_in_rust)

## 2018-07-01
- [Using and_then and map combinators on the Rust Result Type](https://hermanradtke.com/2016/09/12/rust-using-and_then-and-map-combinators-on-result-type.html)

## 2018-07-02
- [Understanding where clauses and trait constraints](https://mgattozzi.github.io/2016/09/13/understanding-where-clauses.html)

## 2018-07-04
- [Exploring Rust fat pointers](https://iandouglasscott.com/2018/05/28/exploring-rust-fat-pointers/)

## 2018-07-05
- [Encapsulating Lifetime of the Field](https://matklad.github.io/2018/05/04/encapsulating-lifetime-of-the-field.html)

## 2018-07-06
- [Non-lexical lifetimes: introduction](http://smallcultfollowing.com/babysteps/blog/2016/04/27/non-lexical-lifetimes-introduction/)

## 2018-07-09
-[类型的大小](https://zhuanlan.zhihu.com/p/21820917)

## Incoming
http://xion.io/post/code/rust-optional-args.html
https://github.com/rust-unofficial/patterns/blob/master/idioms/mem-replace.md
https://aochagavia.github.io/blog/enforcing-drop-order-in-rust/
https://lab.whitequark.org/notes/2016-12-13/abstracting-over-mutability-in-rust/
https://stackoverflow.com/questions/24145823/how-do-i-convert-a-c-string-into-a-rust-string-and-back-via-ffi
https://matklad.github.io/2018/05/24/typed-key-pattern.html



## End
