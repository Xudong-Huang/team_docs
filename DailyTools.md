# Daily used tools

You should have the most suitable tools to improve yourself, to make life better!
------
# Shadowsocks

The first thing is that you should know how to connect world Internet by
[shadowsocks](https://github.com/shadowsocks/shadowsocks-windows/releases)

you should have you own VPS for the server, if you don't have use the company configuration

```json
{
    "server": "54.64.196.139",
    "server_port": 8388,
    "local_address": "127.0.0.1",
    "local_port": 1080,
    "password": "woivNaijOv",
    "timeout": 500,
    "method": "chacha20-ietf-poly1305"
}
```

## advanced usage for kcptun (if you have a shadowsock server)
[kcptun](https://github.com/xtaci/kcptun/releases)


------
# Chrome
a daily used explorer tool you must have

## you should have a google account
create one if you not have

* sync your bookmarks (organize your size)
* sync your chrome settings
* sync your plugins
* can be used in other apps

## Plugins
- Adblock Plus
- Google Translate
- LastPass (Free Password Manager)
- Octotree (Code tree for GitHub)

-----
# Vscode

cross platform, language. with speed and appearance

## Plugins
- Atom One Dark Theme
- vscode-icons
- Better TOML
- Code Outline
- Code Spell Checker
- Dictionary Completion
- EditorConfig for VS Code
- GitLens — Git supercharged
- Instant Markdown
- Path Intellisense
- Settings Sync (save your config in the cloud)
- TODO Highlight
- Vim

## Settings
```json
{
    "editor.fontSize": 13,
    "editor.formatOnSave": true,
    "sync.gist": "60b393f8db6ca32507a2cb76f55ecf17", // place your gist url
    "todohighlight.include": [
        "**/*.js",
        "**/*.jsx",
        "**/*.ts",
        "**/*.html",
        "**/*.php",
        "**/*.css",
        "**/*.scss",
        "**/*.rs"
    ],
    "todohighlight.toggleURI": true,
    "vim.hlsearch": true,
    "vim.easymotion": true,
    "vim.handleKeys": {
        "<C-a>": false,
        "<C-c>": false,
        "<C-v>": false,
        "<C-f>": false,
        "<C-x>": false,
        "<C-t>": false
    },
    "workbench.iconTheme": "vscode-icons",
    "files.associations": {
        "*.toml": "toml",
        "*.lock": "toml"
    },
    "workbench.colorTheme": "Atom One Dark",
    "window.zoomLevel": 2,
    "editor.multiCursorModifier": "ctrlCmd",
    "rust.rustup": {
        "toolchain": "nightly-x86_64-pc-windows-msvc"
    },
    "rust.mode": "legacy",
    "C_Cpp.clang_format_style": "{BasedOnStyle: Chromium, IndentWidth: 4}",
    "gitlens.advanced.messages": {
        "suppressCommitHasNoPreviousCommitWarning": false,
        "suppressCommitNotFoundWarning": false,
        "suppressFileNotUnderSourceControlWarning": false,
        "suppressGitVersionWarning": false,
        "suppressLineUncommittedWarning": false,
        "suppressNoRepositoryWarning": false,
        "suppressResultsExplorerNotice": false,
        "suppressShowKeyBindingsNotice": true,
        "suppressUpdateNotice": false,
        "suppressWelcomeNotice": true
    },
    "[markdown]": {
        "editor.quickSuggestions": true
    },
    // "terminal.integrated.shell.windows": "C:\\cygwin\\bin\\zsh.exe",
    "cSpell.enabledLanguageIds": [
        "c",
        "cpp",
        "csharp",
        "go",
        "handlebars",
        "javascript",
        "javascriptreact",
        "json",
        "latex",
        "markdown",
        "php",
        "plaintext",
        "python",
        "restructuredtext",
        "rust",
        "text",
        "typescript",
        "typescriptreact",
        "yml"
    ],
    "cSpell.language": "en",
    "gitlens.keymap": "chorded",
    "eslint.enable": false,
    "sync.removeExtensions": true,
    "sync.syncExtensions": true,
    "debug.allowBreakpointsEverywhere": true,
    "terminal.integrated.shell.windows": "C:\\WINDOWS\\System32\\cmd.exe",
    "gitlens.historyExplorer.enabled": true,
    "symbolOutline.topLevel": [
        "Namespace",
        "Constant",
        "Struct",
        "Class",
        "Interface",
        "Method",
        "Function"
    ]
}
```

-----
# Vim

You should install vim everywhere and use a native GUI vim

## save your configuration in a git repo
each config change should be recorded
you can sync it again without complicated reconfiguration
[vim-config](https://github.com/dmwp37/dmwp37-vim-config.git)

## Plugins
- VimPlug
- Plug 'jiangmiao/auto-pairs'
- Plug 'Lokaltog/vim-easymotion'
- Plug 'scrooloose/nerdtree'
- Plug 'tpope/vim-surround'
- Plug 'tpope/vim-fugitive'
- Plug 'Shougo/neocomplcache'
- Plug 'bling/vim-airline'
- Plug 'terryma/vim-multiple-cursors'

-----
# git

Try to make everything in a repo and save your change history.

## global config ~/.gitconfig
[我的git配置](https://gist.github.com/Xudong-Huang/391d1921715f8c85e4d86c3dfa88b6bd)

```yaml
[core]
    pager = cat
    ignoreCygwinFSTricks = true
    excludesfile =  ~/.gitignore_global
    filemode = false
[color]
    ui = auto
[alias]
    st = status
    co = checkout
    lsb = branch -a
    br = branch
    lg = log --pretty=oneline --abbrev-commit -18
    df = diff --no-ext-diff
    dfc = diff --no-ext-diff --cached
[http]
    proxy = http://127.0.0.1:1080
[push]
    default = simple
[credential]
    helper = manager
[diff]
    tool = p4diff
[difftool]
    prompt = false
    trustExitCode = false
[difftool "p4diff"]
    cmd = p4merge \"$(cygpath -w \"$LOCAL\")\" \"$(cygpath -w \"$REMOTE\")\"
[merge]
    tool = p4merge
[mergetool "p4merge"]
    cmd = p4merge \"$(cygpath -w \"$BASE\")\" \"$(cygpath -w \"$LOCAL\")\" \"$(cygpath -w \"$REMOTE\")\" \"$(cygpath -w \"$MERGED\")\"
    trustExitCode = true
    keepBackup = false
```

## git diff & git difftool
- [beyond compare 4](https://www.scootersoftware.com/support.php?zz=kb_vcs#gitwindows)

-----
# terminal

## For windows users
- install **cygwin** and **mintty**
  - mirror: http://mirrors.163.com/cygwin/
  - daily tools
    - git
    - vim
    - curl
    - zsh
  - set default zsh (/etc/nsswitch.conf)
  ```sh
  db_shell: /bin/zsh
  ```
  - case sensitive file system
  ```reg
  set HKLM\SYSTEM\CurrentControlSet\Control\Session Manager\kernel\obcaseinsensitive 0
  ```
- you should try to use **WSL** and **ubuntu**
- window10 now support ssh


## For mac users
- **iterm2** is the default choose

-----
# zsh

- [为什么说 zsh 是 shell 中的极品？](https://www.zhihu.com/question/21418449)
- [oh-my-zsh](https://github.com/robbyrussell/oh-my-zsh)
