--------
# Week 1

## Day 1
* introduce company
* team introduction (who/what/how/cultural)
* get computer/network/work site
* setup develop tools (devtools.doc)
* prepare rust learning stuffs (rust new bee.doc)

## Day 2
* meeting root: introduce rust (ppt)
* learning rust by examples ([速成](https://rustwiki.org/zh-CN//rust-by-example/))

## Day 3
有能力建议直接读英文版本
* learning rust book ([中文](https://github.com/KaiserY/rust-book-chinese))
   - basic grammes
   - ownership
   - borrowing
   - lifetime

## Day 4
* continue learning rust more stuffs
   - [r4cpp](https://github.com/nrc/r4cppp)
   - Result/Option/Mpsc/Collections
   - std API

## Day 5
* continue learning rust more stuffs
* rust community stuff
   - ryon/crossbeam/hyper/tokio/future/may

-------
# Week 2

## Day 1
* byteball papare overview
* trustnote js code introduce
* trustnote rust code introduction
* setup trustnote development env
   - how to download code
   - how to commit code
   - how to work with GitHub
   - how to build/test/run
   - how to debug rust code

# Day 2
* assign easy issue for developers
* code review process

-------

